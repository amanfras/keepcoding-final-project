# Keepcoding Final Project

 - Presentación
 - Informe del proyecto

# Análisis R

 - Comparativa con el sentimiento
 - Evolución del bitcoin anual


# Arquitectura

 - Resumen de los pasos de la aplicación y el despliegue en Cloud
 - Notebook con la Arquitectura Bach
 - Notebook con la Arquitectura en Streaming

# Data

 - Datos guardado en cloud Storage, usados para los modelos 

# Entrenamiento

 - Entrenamiento de modelos sentimiento y selección del modelo final
 - Entrenamiento de los modelos de precio del bitcoin a corto y medio plazo, y la selección del modelo óptimo

# Modelos

 - Modelos guardados en Cloud Storage

# Otros Data

 - Dataset no utilizados finalmente 

# Preprocesado

 - EDA

# Web

 - Código de la web de visualización. Divido en dos carpetas Back y Frontend
